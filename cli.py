import argparse
import os
import pandas as pd
from contextlib import contextmanager
import plot

def drop_parens(str):
    return str.replace('(', '').replace(')', '')

def normalize_path(path):
    return os.path.abspath(os.path.expanduser(path))

@contextmanager
def working_directory(path):
    cwd = os.getcwd()
    norm_path = normalize_path(path)
    try:
        os.chdir(norm_path)
        yield
    finally:
        os.chdir(cwd)

# 60days, 24hours, 30minute measurements
def read_data(path,IntMin):
    max_rows=60 * 24 * int(60/IntMin)
    df = pd.read_csv(path, delimiter=',', skiprows=[2, 3], header=1,
                       index_col=0, parse_dates=True, na_values=["NAN", "NaN"])
    df.rename(drop_parens, axis='columns', inplace=True)
    #df = df.resample('10min').asfreq() ## gurgiser want 1min data
    if df.shape[0] > max_rows:
        df = df.truncate(before=df.index[df.shape[0]-max_rows])
    return df


def main():
    parser = argparse.ArgumentParser(description='Create Plots for Llupa AWS')
    parser.add_argument('-o', '--output-dir', default=os.getcwd(), help='Output directory')
    parser.add_argument('data_file', help='Path to the aws data file')
    args = parser.parse_args()
    data_path = normalize_path(args.data_file)
    with working_directory(args.output_dir):
        IntMin=int(30) # measurement interval in min
        data = read_data(data_path,IntMin)
        plot.plot1(data,IntMin)

if __name__ == '__main__':
    main()