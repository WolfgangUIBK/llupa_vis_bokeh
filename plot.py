# -*- coding: utf-8 -*-
"""
Created on Thu Dec  6 09:39:33 2018

@author: Wolfgang Gurgiser, Christian Posch
"""

import numpy as np
import pandas as pd
import math
from math import pi
from datetime import datetime, timedelta
from bokeh.io import output_file, save
from bokeh.layouts import gridplot
from bokeh.plotting import figure
from bokeh.models import ColumnDataSource,DatetimeTickFormatter, NumeralTickFormatter, HoverTool, LinearAxis, Range1d
from bokeh.models.glyphs import Rect
from bokeh.models import Label

def calc_diff_last_data_2_now(df):
    return datetime.utcnow() - timedelta(hours=5) - df.index[-1]

def style_figure(f):
    f.background_fill_color = "black"
    f.border_fill_color="black"
    f.axis.axis_line_color = "white"
    f.grid.grid_line_color = "#595959"
    f.axis.major_label_text_color = "white"
    f.axis.axis_label_text_color = "white"
    f.title.text_color="white"
    f.legend.background_fill_color="black"
    f.legend.background_fill_alpha = 1
    f.legend.border_line_color = "black"
    f.legend.label_text_color = "white"
    f.legend.location = "bottom_left"
    f.xaxis.formatter=DatetimeTickFormatter(
        hours=["%d %B %Y"],
        days=["%Y %m %d"],
        months=["%d %B %Y"],
        years=["%Y"],
    )
    f.xaxis.major_label_orientation = pi/4

def plot1(df,IntMin):
    output_file('overview_aws_llupa_3days.html')
    delta=-int(60/IntMin) * 3 * 24
    f1 = plot1_figure1(df, delta,IntMin)
    f2 = plot1_figure2(df, delta,IntMin)
    f3 = plot1_figure3(df, delta,IntMin)
    f4 = plot1_figure4(df, delta,IntMin)
    grid = gridplot([f1, f2, f3, f4], ncols=2, plot_width=600, plot_height=600)
    save(grid)
    from bokeh.plotting import show
    show(grid)

def hover_tool():
    return HoverTool(tooltips=[
        ('Date', '@x{%Y %m %d %Hh}'),
        ('Value', '@y{1.11}')], formatters={'x': 'datetime'}, mode='mouse')

def create_figure(label, df, delta):
    f = figure(plot_width=300, plot_height=300, x_axis_type='datetime',
               title=label)
    f.x_range.start = pd.Timestamp.now() - timedelta(days=3)
    f.x_range.end = pd.Timestamp.now()
    return f

def calc_font_size(time_diff):
    if time_diff > timedelta(hours=10):
        return '20pt'
    return '10pt'

def add_blue_rect(f, df, time_diff, mmin, mmax, height):
    middle = (mmax + mmin)/2
    width = calc_diff_last_data_2_now(df)/2
    f.rect([df.index[-1]+width], [0], time_diff, height, fill_alpha=0.25)
    l = Label(x=pd.Timestamp.now()-timedelta(hours=1), y=middle, text='NO DATA', 
            text_font_size=calc_font_size(time_diff), 
            text_color='white', text_align="center", 
            text_alpha=0.80, angle=90, angle_units='deg')
    f.add_layout(l)

def add_total_rain_label(f, val12, val24, posx=480, posy=480):
    mfontsize = '10pt'
    secposdiff = 25
    l = Label(x=posx, y=posy, text='Total precipitation last 12h: {:.2f} mm'.format(round(val12,2)), 
            text_font_size=mfontsize, x_units='screen', y_units='screen',
            text_color='yellow', text_align="right", 
            text_alpha=0.80, angle=0, angle_units='deg')
    f.add_layout(l)
    l = Label(x=posx, y=posy-secposdiff, text='Total Precipitation last 24h: {:.2f} mm'.format(round(val24,2)), 
            text_font_size=mfontsize, x_units='screen', y_units='screen',
            text_color='yellow', text_align="right", 
            text_alpha=0.80, angle=0, angle_units='deg')
    f.add_layout(l)

def calc_rain_total(df, mhours,IntMin):
    #print(df.index[-1*mhours*60])
    return sum(df[-1*mhours*int(60/IntMin):].Rain_mm_Tot)

def calc_real_range(df, delta,IntMin):
    mdays = math.ceil(delta*-1/24/(60/IntMin))
    print(math.ceil(delta*-1/24/(60/IntMin)))
    time_diff = calc_diff_last_data_2_now(df)
    print(time_diff)
    time_diff_in_data = timedelta(days=mdays) - time_diff
    print(time_diff_in_data)
    diff_4_range_check = math.ceil(time_diff_in_data.days * 24 * 60/IntMin + time_diff_in_data.seconds/60/IntMin) * -1
    print(diff_4_range_check)
    return diff_4_range_check

def plot1_figure1(df, delta,IntMin):
    diff_4_range_check = calc_real_range(df, delta,IntMin)
    f = create_figure('Temperature and Humidity', df, delta)
    t_minima = df[diff_4_range_check:].AirTC_Avg.min() 
    t_maxima = df[diff_4_range_check:].AirTC_Avg.max()
    f.y_range = Range1d(start=math.floor(t_minima - 1), end=math.ceil(t_maxima + 1))
    f.extra_y_ranges = {'Humidity': Range1d(start=df.RH.min(), end=df.RH.max())}
    f.add_tools(hover_tool())
    f.line(df.index, df.AirTC_Avg, line_width=2, color='white', legend_label='T Air')
    f.line(df.index, df.RH, line_width=2, y_range_name="Humidity", line_dash='dashed', color='#27f7f3', legend_label='RH')
    f.yaxis.axis_label = 'T'
    f.add_layout(LinearAxis(y_range_name='Humidity', axis_label='%'), 'right')   
 
    if calc_diff_last_data_2_now(df) > timedelta(hours=2):
        #add_blue_rect(f, df, calc_diff_last_data_2_now(df), t_minima.min(), t_maxima.max(), 210)
        add_blue_rect(f, df, calc_diff_last_data_2_now(df), t_minima, t_maxima, 210)

    style_figure(f)
    return f

def plot1_figure2(df, delta,IntMin):
    diff_4_range_check = calc_real_range(df, delta,IntMin)
    f = create_figure('Wind', df, delta)
    f.extra_y_ranges = {'Wind_dir': Range1d(start=-0.5, end=365)}
    f.y_range = Range1d(start=-0.5, end=df[diff_4_range_check:].WS_ms_Max.max() + 0.5)
    # print(diff_4_range_check)
    # print(df[diff_4_range_check:].WS_ms_Max)
    # print(df[diff_4_range_check:].WS_ms_Max.max())
    f.add_tools(hover_tool())
    f.circle(df.index, df.WindDir_D1_WVT, radius=1, radius_dimension='max', radius_units='data', color='#27f7f3', y_range_name='Wind_dir',
             legend_label='Wind dir', name='Wind_dir')
    f.line(df.index, round(df.WS_ms_S_WVT*100)/100, line_width=1, color='white', legend_label='ws')
    f.line(df.index, round(df.WS_ms_Max*100)/100, line_width=1, color='grey', legend_label='ws max')
    f.yaxis.axis_label = 'm/s'
    f.add_layout(LinearAxis(y_range_name='Wind_dir', axis_label='degree'), 'right')

    if calc_diff_last_data_2_now(df) > timedelta(hours=2):
        add_blue_rect(f, df, calc_diff_last_data_2_now(df), 0, df.WS_ms_S_WVT[delta:].max(), 510)

    style_figure(f)
    return f

def plot1_figure3(df, delta,IntMin):
    diff_4_range_check = calc_real_range(df, delta,IntMin)

    f = create_figure("Precipitation intensity", df, delta)
    f.y_range = Range1d(start=-0.1, end=df[diff_4_range_check:].Rain_mm_Tot.max() + 0.2)
    SoilHumidity_max_vector=np.array([df.VWC15cm_Avg[diff_4_range_check:-1].max(),df.VWC35cm_Avg[diff_4_range_check:-1].max(),df.VWC60cm_Avg[diff_4_range_check:-1].max()])
    #SoilHumidity_min_vector=np.array([df.VWC15cm_Avg[diff_4_range_check:-1].min(),df.VWC35cm_Avg[diff_4_range_check:-1].min(),df.VWC60cm_Avg[diff_4_range_check:-1].min()])
    f.extra_y_ranges = {'Soil Humidity': Range1d(start=0, end=SoilHumidity_max_vector.max()+0.02)}
    f.add_tools(hover_tool())
    f.line(df.index, df.Rain_mm_Tot, line_width=2, color='#27f7f4', line_dash='dashed', legend_label='precipitation')
    f.circle(df[df['Rain_mm_Tot'] >0].index,df[df['Rain_mm_Tot'] >0].Rain_mm_Tot,radius=0.025,radius_dimension='max', radius_units='data',fill_color='#27f7f4',color='#27f7f4')
    f.line(df.index, df.VWC15cm_Avg, line_width=2, line_dash='dashed', y_range_name="Soil Humidity", color='white', legend_label='15cm')
    f.line(df.index, df.VWC35cm_Avg, line_width=2, line_dash='dotted', y_range_name="Soil Humidity", color='white', legend_label='35cm')
    f.line(df.index, df.VWC60cm_Avg, line_width=2, line_dash='dotdash', y_range_name="Soil Humidity", color='white', legend_label='60cm')
    f.yaxis.axis_label = 'mm/' + str(IntMin) + ' min'
    f.add_layout(LinearAxis(y_range_name='Soil Humidity', axis_label='VWC (m^3/m^3)'), 'right') 
    
    if calc_diff_last_data_2_now(df) < timedelta(minutes=80): # 80min beacause, 1hour timediff+20min update sourcedatafile
        add_total_rain_label(f, calc_rain_total(df, 12), calc_rain_total(df, 24))

    if calc_diff_last_data_2_now(df) > timedelta(hours=2):
        add_blue_rect(f, df, calc_diff_last_data_2_now(df), 0, df.Rain_mm_Tot[delta:].max(), 510)

    style_figure(f)
    return f

def plot1_figure4(df, delta,IntMin):
    diff_4_range_check = calc_real_range(df, delta,IntMin)
    # Figure 3
    f = create_figure("Radiation", df, delta)
    # correct_radiation_minima(df, -5, 0)
    # correct_radiation_maxima(df, 1500, 500)
    # correct_nighttime_shortwave_radiation(df, 20)
    f.extra_y_ranges = {"Albedo": Range1d(0,1)}

    # Setting the second y axis range name and range
    # Find min and max of all Radiation Variables for Y-axis limits
    Rad_min_vector=np.array([-df.SWout_Avg[diff_4_range_check:-1].max(),-df.LWout_Avg[diff_4_range_check:-1].max()])# minus sign gets only active after .max operation
    Rad_max_vector=np.array([df.SWin_Avg[diff_4_range_check:-1].max(),df.LWin_Avg[diff_4_range_check:-1].max()])
    # f.y_range = Range1d(math.floor(Rad_min_vector[delta:-1].min()-10), math.ceil(Rad_max_vector[delta:-1].max()+10))
    f.y_range = Range1d(math.floor(Rad_min_vector.min()-10), math.ceil(Rad_max_vector.max()+10))
    f.add_tools(hover_tool())

    # Add line data
    #f3.line(df.index, Albedo, line_dash="dashed",color="#27f7f3", y_range_name="Albedo",legend="Albedo",name="Albedo")
    f.line(df.index, df.SWin_Avg, line_width=1,color="white",legend_label="SWin/out")
    f.line(df.index, -df.SWout_Avg, line_width=1,color="white")
    f.line(df.index, df.LWin_Avg, line_width=1,color="orange",legend_label="LWin/out")
    f.line(df.index, -df.LWout_Avg, line_width=1,color="orange")
    f.line(df.index, df.SWin_Avg-df.SWout_Avg+df.LWin_Avg-df.LWout_Avg, line_width=1,color="red",legend_label="LWnet")
    # add labels
    f.yaxis.axis_label = "W/m²"

    #SABO stuff
    time_diff = calc_diff_last_data_2_now(df)
    if time_diff > timedelta(hours=2):
        add_blue_rect(f, df, time_diff, Rad_min_vector[delta:-1].min(), Rad_max_vector[delta:-1].max(), math.floor(Rad_max_vector[delta:-1].max()*2.1))
        #middle = math.floor((Rad_max_vector[delta:-1].max() + Rad_min_vector[delta:-1].min())/2)
        #f.rect([df.index[-1]+calc_diff_last_data_2_now(df)/2], [0], calc_diff_last_data_2_now(df), math.floor(Rad_max_vector[delta:-1].max()*2.1), fill_alpha=0.25)
        #l = Label(x=df.index[-1]+calc_diff_last_data_2_now(df)/2, y=middle, text='DATA IS MISSING', text_font_size=calc_font_size(time_diff), 
        #    text_color='white', text_align="center", text_alpha=0.80, angle=90, angle_units='deg')
        #f.add_layout(l)

    style_figure(f)
    return f

