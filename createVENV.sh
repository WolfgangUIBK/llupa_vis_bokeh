#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Input argument PROJECTNAME needed!"
    exit
fi

PRJN="$1_venv"
python3 -m venv ${PRJN}
${PRJN}/bin/pip3 install -r requirements.txt
${PRJN}/bin/pip3 install -e .

